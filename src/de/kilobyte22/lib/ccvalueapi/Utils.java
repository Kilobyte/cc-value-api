package de.kilobyte22.lib.ccvalueapi;

public class Utils {
    public static int getIndex(Object[] haystack, Object needle) {
        for (int i = 0; i < haystack.length; i++) {
            if (haystack[i].equals(needle))
                return i;
        }
        return -1;
    }

    public static int getIndex(String[] haystack, String needle) {
        for (int i = 0; i < haystack.length; i++) {
            if (haystack[i].equals(needle))
                return i;
        }
        return -1;
    }

    public static String safeToString(Object o) {
        if(o == null)
            return "null";
        return o.toString();
    }

    public static boolean safeEquals(Object o, Object o1) {
        if (o1 == null) return o == null;
        return o1.equals(o);
    }
}
