package de.kilobyte22.lib.ccvalueapi.tileentity;

import dan200.computer.api.IComputerAccess;
import dan200.computer.api.IPeripheral;
import de.kilobyte22.lib.ccvalueapi.values.LuaValue;
import de.kilobyte22.lib.ccvalueapi.reflection.PeripheralFunction;
import de.kilobyte22.lib.ccvalueapi.LuaError;
import de.kilobyte22.lib.ccvalueapi.values.Varargs;
import net.minecraft.tileentity.TileEntity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;

public abstract class PeripheralTileEntity extends TileEntity implements IPeripheral {

    protected LinkedList<IComputerAccess> attachedComputers = new LinkedList<IComputerAccess>();

    @Override
    public final String[] getMethodNames() {
        ArrayList<String> ret = new ArrayList<String>();
        for (Method m : this.getClass().getMethods()) {
            PeripheralFunction ann = m.getAnnotation(PeripheralFunction.class);
            if (ann != null) {
                ret.add(ann.value());
            }
        }
        return ret.toArray(new String[0]);
    }

    @Override
    public final Object[] callMethod(IComputerAccess computer, int method, Object[] arguments) throws Exception {
        try {
            try {
                for (Method m : this.getClass().getMethods()) {
                    PeripheralFunction ann = m.getAnnotation(PeripheralFunction.class);
                    if (ann != null && ann.value().equals(getMethodNames()[method])) {
                        Varargs ret = (Varargs) m.invoke(this, computer, new Varargs(arguments));
                        if (ret != null)
                            return ret.toArrayOfObject();
                        return new Object[0];
                    }
                }
                return new Object[0];
            } catch (InvocationTargetException e) {
                throw e.getCause();
            }
        } catch (LuaError error) {
            throw new Exception(error.getMessage());
        } catch (Throwable ex) {
            System.err.println("VM Error when calling " + getMethodNames()[method] + " on " + getType() + ":");
            ex.printStackTrace();
            Exception ex_ = new Exception("vm error: " + ex.getClass().getName() + ": " + ex.getMessage());
            ex_.initCause(ex);
            throw ex_;
        }
    }

    @Override
    public boolean canAttachToSide(int side) {
        return true;
    }

    @Override
    public void attach(IComputerAccess computer) {
        attachedComputers.add(computer);
    }

    @Override
    public void detach(IComputerAccess computer) {
        attachedComputers.remove(computer);
    }

    protected void broadcastEvent(String type, Object data1, Object... data) {
        Object[] params = new Object[data.length + 2];
        params[1] = data1;
        for (int i = 0; i < data.length; i++) {
            params[i + 2] = data[i];
        }

        for (IComputerAccess computer : attachedComputers) {
            params[0] = computer.getAttachmentName();
            computer.queueEvent(type, params);
        }
    }

    protected void broadcastEvent(String type, LuaValue data1, LuaValue... data) {
        Object[] params = new Object[data.length + 2];
        params[1] = data1.toObject();
        for (int i = 0; i < data.length; i++) {
            params[i + 2] = data[i].toObject();
        }

        for (IComputerAccess computer : attachedComputers) {
            params[0] = computer.getAttachmentName();
            computer.queueEvent(type, params);
        }
    }

    protected void broadcastEvent(String type, Varargs args) {
        Object[] a = args.toArrayOfObject();
        Object[] params = new Object[a.length + 1];
        for (int i = 0; i < a.length; i++) {
            params[i + 1] = a[i];
        }
        for (IComputerAccess computer : attachedComputers) {
            params[0] = computer.getAttachmentName();
            computer.queueEvent(type, params);
        }
    }

    protected void broadcastEvent(String type) {
        for (IComputerAccess computer : attachedComputers) {
            computer.queueEvent(type, new Object[]{computer.getAttachmentName()});
        }
    }
}
