package de.kilobyte22.lib.ccvalueapi.values;

public class LuaNil extends LuaValue {

    @Override
    public Object toObject() {
        return null;
    }

    @Override
    public String getType() {
        return "nil";
    }
}
