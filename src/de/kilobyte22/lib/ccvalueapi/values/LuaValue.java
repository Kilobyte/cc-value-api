package de.kilobyte22.lib.ccvalueapi.values;

import de.kilobyte22.lib.ccvalueapi.LuaError;
import de.kilobyte22.lib.ccvalueapi.Utils;

import java.util.HashMap;
import java.util.WeakHashMap;

public abstract class LuaValue {

    public static final LuaNil NIL = new LuaNil();
    public static final LuaBoolean FALSE = new LuaBoolean(false);
    public static final LuaBoolean TRUE = new LuaBoolean(true);

    private static WeakHashMap<Object, LuaValue> cache = new WeakHashMap<Object, LuaValue>();

    public abstract Object toObject();
    public abstract String getType();

    public static LuaValue valueOf(Object o) {
        LuaValue ret = null;
        //if (cache.containsKey(o))
        //    return cache.get(o);
        if (o instanceof HashMap) {
            ret = new LuaTable((HashMap<Object, Object>) o);
        } else if (o instanceof String) {
            ret = new LuaString((String) o);
        } else if (o instanceof Double) {
            ret = new LuaNumber((Double) o);
        } else if(o instanceof Integer) {
            ret = new LuaNumber((double) ((Integer) o));
        } else if (o instanceof Boolean) {
            ret = ((Boolean) o ? TRUE : FALSE);
        } else if(o == null) {
            ret = NIL;
        } else if (o instanceof Object[]) {
            LuaTable ret_ = new LuaTable();
            for (int i = 0; i < ((Object[]) o).length; i++) {
                ret_.set(new LuaNumber((double) i + 1), LuaValue.valueOf(((Object[])o)[i]));
            }
        } else {
            ret = new LuaUserdata(o);
        }

        cache.put(o, ret);
        return ret;
    }

    @Override
    public String toString() {
        return getType() + Utils.safeToString(toObject());
    }

    public int length() {
        throw new LuaError("Attempt to get length of " + getType());
    }

    public String checkstring() {
        if (getType().equals("string"))
            return (String) toObject();
        throw new LuaError("string expected, got " + getType());
    }

    public Double checknumber() {
        if (getType().equals("number"))
            return (Double) toObject();
        throw new LuaError("number expected, got " + getType());
    }

    public Boolean checkboolean() {
        if (getType().equals("boolean"))
            return (Boolean) toObject();
        throw new LuaError("boolean expected, got " + getType());
    }

    public Integer checkint() {
        if (getType().equals("number"))
            return ((Double) toObject()).intValue();
        throw new LuaError("number expected, got " + getType());
    }

    public LuaTable checktable() {
        if (getType().equals("table"))
            return (LuaTable) this;
        throw new LuaError("table expected, got " + getType());
    }

    public String optstring(String def) {
        if (getType().equals("string"))
            return (String) toObject();
        if (getType().equals("nil"))
            return def;
        throw new LuaError("string or nil expected, got " + getType());
    }

    public int optint(int def) {
        if (getType().equals("number"))
            return ((Double) toObject()).intValue();
        if (getType().equals("nil"))
            return def;
        throw new LuaError("number or nil expected, got " + getType());
    }

    public boolean optbool(boolean def) {
        if (getType().equals("boolean"))
            return (Boolean) toObject();
        if (getType().equals("nil"))
            return def;
        throw new LuaError("boolean or nil expected, got " + getType());
    }
}
