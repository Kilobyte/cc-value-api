package de.kilobyte22.lib.ccvalueapi.values;

public class LuaUserdata extends LuaValue {

    private Object data;

    public LuaUserdata(Object o) {
        data = o;
    }

    @Override
    public Object toObject() {
        return data;
    }

    @Override
    public String getType() {
        return "userdata";
    }
}
