package de.kilobyte22.lib.ccvalueapi.values;

public class LuaBoolean extends LuaValue {
    private final Boolean data;

    public LuaBoolean(Boolean o) {
        if (o == null)
            throw new NullPointerException("Can't create nullboolean");
        data = o;
    }

    @Override
    public Object toObject() {
        return data;
    }

    @Override
    public String getType() {
        return "boolean";
    }
}
