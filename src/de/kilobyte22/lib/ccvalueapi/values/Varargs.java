package de.kilobyte22.lib.ccvalueapi.values;

public class Varargs {
    private LuaTable data;

    public Varargs(Object... data) {
        this.data = new LuaTable();
        for (int i = 0; i < data.length; i++) {
            this.data.set(LuaValue.valueOf(i + 1), LuaValue.valueOf(data[i]));
        }
    }

    public String checkstring(int param) {
        return data.get(param).checkstring();
    }

    public Double checknumber (int param) {
        return data.get(param).checknumber();
    }

    public Boolean checkboolean(int param) {
        return data.get(param).checkboolean();
    }

    public Integer checkint(int param) {
        return data.get(param).checkint();
    }

    public String optstring(int param, String def) {
        return data.get(param).optstring(def);
    }

    public int optint(int param, int def) {
        return data.get(param).optint(def);
    }

    public boolean optbool(int param, boolean def) {
        return data.get(param).optbool(def);
    }

    public Object[] toArrayOfObject() {
        Object[] ret = new Object[data.length()];
        for (LuaValue k : data.keySet()) {
            ret[((int) ((double) ((Double) k.toObject()))) - 1] = data.get(k).toObject();
        }
        return ret;
    }
}
