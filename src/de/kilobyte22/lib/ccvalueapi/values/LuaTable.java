package de.kilobyte22.lib.ccvalueapi.values;

import de.kilobyte22.lib.ccvalueapi.Utils;

import java.util.HashMap;
import java.util.Set;

public class LuaTable extends LuaValue {

    private HashMap<LuaValue, LuaValue> data = new HashMap<LuaValue, LuaValue>();

    public LuaTable(HashMap<Object, Object> o) {
        for (Object k : o.keySet()) {
            data.put(valueOf(k), valueOf(o.get(k)));
        }
    }

    public LuaTable() {

    }

    @Override
    public Object toObject() {
        HashMap<Object, Object> ret = new HashMap<Object, Object>();
        for (LuaValue value : data.keySet()) {
            ret.put(value.toObject(), data.get(value.toObject()));
        }
        return ret;
    }

    @Override
    public String getType() {
        return "table";
    }

    @Override
    public int length() {
        return data.size();
    }

    public void set(LuaValue key, LuaValue value) {
        data.put(key, value);
    }

    public LuaValue get(LuaValue key) {
        LuaValue ret = null;
        for (LuaValue k : data.keySet()) {
            if (Utils.safeEquals(k.toObject(), key.toObject()))
                ret = data.get(k);
        }
        if (ret == null) ret = NIL;
        return ret;
    }

    public LuaValue get(int key) {
        return get(new LuaNumber((double) key));
    }

    public Set<LuaValue> keySet() {
        return data.keySet();
    }

    @Override
    public LuaTable checktable() {
        return this;
    }
}
