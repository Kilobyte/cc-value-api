package de.kilobyte22.lib.ccvalueapi.values;

public class LuaNumber extends LuaValue {

    private Double data;

    public LuaNumber(Double o) {
        if (o == null)
            throw new NullPointerException("Can't create nullnumber");
        data = o;
    }

    @Override
    public Object toObject() {
        return data;
    }

    @Override
    public String getType() {
        return "number";
    }
}
