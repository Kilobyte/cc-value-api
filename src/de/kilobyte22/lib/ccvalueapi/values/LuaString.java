package de.kilobyte22.lib.ccvalueapi.values;

public class LuaString extends LuaValue {

    private String data;

    public LuaString(String s) {
        if (s == null)
            throw new NullPointerException("Can't create nullstring");
        data = s;
    }

    @Override
    public int length() {
        return data.length();
    }

    @Override
    public Object toObject() {
        return data;
    }

    @Override
    public String getType() {
        return "string";
    }

    @Override
    public String checkstring() {
        return data;
    }
}
