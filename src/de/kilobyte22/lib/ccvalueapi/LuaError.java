package de.kilobyte22.lib.ccvalueapi;

public class LuaError extends RuntimeException {

    public LuaError(String s) {
        super(s);
    }
}
