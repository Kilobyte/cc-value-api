# CC Value API

## Usage
Basic Peripheral

    import dan200.computer.api.IComputerAccess;
    import de.kilobyte22.lib.ccvalueapi.tileentity.PeripheralTileEntity;
    import de.kilobyte22.lib.ccvalueapi.reflection.PeripheralFunction;
    import de.kilobyte22.lib.ccvalueapi.LuaError;
    import de.kilobyte22.lib.ccvalueapi.values.Varargs;

    public class TileEntityMyPeripheral extends PeripheralTileEntity {

        @Override
        public String getType() {
            return "myPeripheral";
        }

        @PeripheralFunction("myFirstFunction") // The name this function will be called with ingame
        public Varargs function1(IComputerAccess computer, Varargs args) { 
            // The function must have the param types IComputerAccess, Varargs
            // and has to return Varargs

            //What it will do: accept 2 strings and combine them (basicly string1..string2 in lua)
            return new Varargs(args.checkstring(1) + args.checkstring(2));
        }

        @PeripheralFunction("getHealth")
        public Varargs iwantzhealth(IComputerAccess computer, Varargs args) {
            // returns the players health or an error if player is not in current world
            EntityPlayer entityPlayer = worldObj.getPlayerEntityByName(args.checkstring(1));
            if (entityPlayer == null)
                throw new LuaError("Player offline or in different world"); // This will arrive in Lua as if you used error()
            return new Varargs(entityPlayer.getHealth());
        }

        @PeripheralFunction("broadcastAttached")
        public Varargs broadcastAttached(IComputerAccess computer, Varargs args) {
            // broadcast the event. INFO: it will always pass the side the peripheral is attached to as first param
            broadcastEvent("event_broadcast", args); // broadcastEvent() actually has 4 overloadings, check them out in the source
            return new Varargs();
        }
    }

## License
I don't mind you using it. HOWEVER i really would appreciate a small link to this repository from your Peripheral Thread on CCForums (or your Mod Thread, no matter where hosted). Also, in case you change the package name it has to contain the name kilobyte and must be in a dedicated package. Youmay also create your files there, as long as they are directly related to the API. DO NOT CHANGE ANY CODE UNLESS YOU CHANGED THE PACKAGE NAME. EVERYTHING ELSE MIGHT LEAD TO CONFLICTS

Also, you may not redistribute this code except with your mods or with your mods source.

A written permission can allow exceptions to any of the rules. This license may be changed at any point without announcement